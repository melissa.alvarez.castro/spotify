import requests as req
import pandas as pd
import psycopg2
from datetime import datetime, timezone
from sqlalchemy import create_engine

CLIENT_ID = '4acbc5f9fd6d4b6e863b2ea45756dda9'
CLIENT_SECRET = '0c3538f5487849f29ab1efffc3703ccd'
AUTH_URL = 'https://accounts.spotify.com/api/token'

# POST
auth_response = req.post(AUTH_URL, {
    'grant_type': 'client_credentials',
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET,
})

# convert the response to JSON
auth_response_data = auth_response.json()

# save the access token
access_token = auth_response_data['access_token']

headers = {
    'Authorization': 'Bearer {token}'.format(token=access_token)
}

list_artist_json = []
list_artist_tracks_json = []

def readArtist(artist_id):
    # GET request Artist
    res_artist = req.get(BASE_URL + '/artists/' + artist_id, headers=headers)
    res_artist = res_artist.json()

    dt = datetime.utcnow()

    res_artist.update({
        'loading_date': dt.replace(tzinfo=timezone.utc).timestamp(),
        'origin': 'Api de Spotify',
        'followers': res_artist['followers']['total'],
    })

    list_artist_json.append(res_artist)

    # GET request Album Artist
    res_album = req.get(BASE_URL + '/artists/' + artist_id + '/albums', headers=headers, params={'include_groups': 'album', 'limit': 10})
    response_album = res_album.json()['items']

    for album in response_album:
        res_album_tracks = req.get(BASE_URL + '/albums/' + album['id'] + '/tracks', headers=headers)
        response_album_tracks = res_album_tracks.json()['items']

        for track in response_album_tracks:
            res_tracks = req.get(BASE_URL + '/tracks/' + track['id'], headers=headers)
            response_tracks = res_tracks.json()

            dt = datetime.utcnow()

            response_tracks.update({
                'loading_date': dt.replace(tzinfo=timezone.utc).timestamp(),
                'origin': 'Api de Spotify',
                'artist': res_artist['name'],
                'release_date': album['release_date'],
                'album': album['name'],
                'genres': res_artist['genres'],
            })

            list_artist_tracks_json.append(response_tracks)

# base URL of all Spotify API endpoints
BASE_URL = 'https://api.spotify.com/v1'

# Artists ID from the URI
artist_ids = ['2ye2Wgw4gimLv2eAKyk1NB', '5M52tdBnJaKSvOpJGz8mfZ', '36QJpDe2go2KgaRleHCDTp', '74ASZWbe4lXaubB36ztrGX', '3fMbdgg4jU18AjLCKBhRSm']

for artist in artist_ids:
    readArtist(artist)

data_df_artists = pd.DataFrame(list_artist_json)
data_to_db_artists = data_df_artists[['name','popularity','type','uri','followers', 'origin', 'loading_date']]

data_df_tracks = pd.DataFrame(list_artist_tracks_json)
print(data_df_tracks['loading_date'])
data_to_db_tracks = data_df_tracks[['name', 'type', 'artist', 'album', 'track_number', 'popularity', 'id', 'uri', 'release_date', 'genres', 'origin', 'loading_date']]



# Load to DB
engine = create_engine('postgresql://postgres:123456@127.0.0.1:5432/postgres')
data_to_db_artists.to_sql('artist', con=engine, index=False)
data_to_db_tracks.to_sql('track', con=engine, index=False)