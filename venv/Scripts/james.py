import requests
import requests as req
import pandas as pd
import psycopg2

from sqlalchemy import create_engine

CLIENT_ID = '4acbc5f9fd6d4b6e863b2ea45756dda9'
CLIENT_SECRET = '0c3538f5487849f29ab1efffc3703ccd'
AUTH_URL = 'https://accounts.spotify.com/api/token'

# POST
auth_response = req.post(AUTH_URL, {
    'grant_type': 'client_credentials',
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET,
})

# convert the response to JSON
auth_response_data = auth_response.json()

# save the access token
access_token = auth_response_data['access_token']

headers = {
    'Authorization': 'Bearer {token}'.format(token=access_token)
}

artistas_json = []
data = []
albums = []

def readArtist(artist_id):
    # GET request with proper header
    res_artist = req.get(BASE_URL + 'artists/' + artist_id, headers=headers)
    res_artist = res_artist.json()
    res_artist.update({
        'followers': res_artist['followers']['total']
    })
    artistas_json.append(res_artist)

    # GET request with proper header

    r = requests.get(BASE_URL + 'artists/' + artist_id + '/albums', headers=headers, params={'include_groups': 'album', 'limit': 1})
    d = r.json()
    for album in d['items']:
        album_name = album['name']

        # pull all tracks from this album
        r = requests.get(BASE_URL + 'albums/' + album['id'] + '/tracks',
                         headers=headers)
        tracks = r.json()['items']

        for track in tracks:
            # get audio features (key, liveness, danceability, ...)
            f = requests.get(BASE_URL + 'tracks/' + track['id'],
                             headers=headers)
            f = f.json()

            f.update({
                'artists': res_artist['name'],
                'release_date': album['release_date'],
                'album': album_name,
                'genres': res_artist['genres']
            })
            data.append(f)


# base URL of all Spotify API endpoints
BASE_URL = 'https://api.spotify.com/v1/'

# Artists ID from the URI
artist_ids = ['2ye2Wgw4gimLv2eAKyk1NB']

for artist in artist_ids:
    readArtist(artist)

data_df_artist = pd.DataFrame(artistas_json)
data_to_db = data_df_artist[['followers', 'name', 'popularity', 'type', 'uri']]

data_df_track = pd.DataFrame(data)
data_to_db2 = data_df_track[['name', 'type', 'artists', 'album','track_number', 'popularity', 'id', 'uri', 'release_date', 'genres']]


# Load to DB
engine = create_engine('postgresql://postgres:123456@127.0.0.1:5432/spotify')
data_to_db.to_sql('artist', con=engine, index=False)
data_to_db2.to_sql('track', con=engine, index=False)